import Home from '@/components/Home.vue';
class ClassToExtend {
  protected abc = 'def';
  num = 123;
}

export default class TestClass extends ClassToExtend {
  protected def = 'ghi';
  num2 = 456;
}

const main = () => {
  let tc: TestClass;
  const tc = new TestClass();
  console.log(tc.abc);
  console.log(tc.num);
  console.log(tc.def);
  console.log(tc.num2);

  let hm: Home;
  hm = new Home();
};

main();
