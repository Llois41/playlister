"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
exports.__esModule = true;
var ClassToExtend = /** @class */ (function () {
    function ClassToExtend() {
        this.abc = 'def';
        this.num = 123;
    }
    return ClassToExtend;
}());
var TestClass = /** @class */ (function (_super) {
    __extends(TestClass, _super);
    function TestClass() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.def = 'ghi';
        _this.num2 = 456;
        return _this;
    }
    return TestClass;
}(ClassToExtend));
exports["default"] = TestClass;
var main = function () {
    var tc = new TestClass();
    console.log(tc.abc);
    console.log(tc.num);
    console.log(tc.def);
    console.log(tc.num2);
};
main();
