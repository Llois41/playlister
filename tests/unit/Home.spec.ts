import { Wrapper, shallowMount } from '@vue/test-utils';
import Home from '@/components/Home.vue';

describe('Home.vue', () => {
  let home: Home;
  let wrapper: Wrapper<Home>;

  beforeEach(async () => {
    home = new Home();
    wrapper = shallowMount(Home);
    wrapper.vm.$data.welcomeMsg = 1234;
    console.log(home);
    home.$mount();
  });

  afterEach(() => {
    home.$destroy();
    //home.welcomeMsg;
  });

  it('should show welcome message', async () => {
    const welcomeMsg = 'Welcome to my App';
    home.$data.welcomeMsg = 123;
    await home.$nextTick();
    expect(home.$el.querySelector('.welcome-message')?.textContent).toEqual(
      welcomeMsg,
    );
  });
});

//it('should show user-menu', () => {});
